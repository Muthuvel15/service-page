import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appImageColor]'
})
export class ImageColorDirective {

  constructor(private imageColor: ElementRef) {
   this.imageColor.nativeElement.stylestyle.color ="blue"
   }

}
