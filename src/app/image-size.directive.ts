import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appImageSize]'
})
export class ImageSizeDirective {

  constructor(private imageSize: ElementRef) {
    this.imageSize.nativeElement.style.width = '20 px';
    this.imageSize.nativeElement.style.height = '10 px';
  }

}
