import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ServicepageComponent } from './servicepage/servicepage.component';
import { ImageSizeDirective } from './image-size.directive';
import { ProgrammingLanguageTableComponent } from './programming-language-table/programming-language-table.component';
import { ProgLangComponent } from './prog-lang/prog-lang.component';
import { ImageColorDirective } from './image-color.directive';
import { LandingPageComponent } from './landing-page/landing-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ServicepageComponent,
    ImageSizeDirective,
    ProgrammingLanguageTableComponent,
    ProgLangComponent,
    ImageColorDirective,
    LandingPageComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
